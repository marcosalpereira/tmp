import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const DADOS_URL = environment.dadosUrl;

export interface TipoMetrica {
  id: string;
  descricao: string;
  principal?: boolean;
  icone?: string;
}
export interface OutrosDados {
  codigoSistema: string;
  siglaSistema: string;
  nomeSistema: string;
  cliente: string;
}

export declare type Metrica = {
  [id in string]: number;
}
export interface Item {
  id: string;
  idItemPai: string;
  nome: string;
  nomeGrupo: string;
  metricas: Metrica;
  outrosDados?: OutrosDados;
  totais?: boolean;
  temFilhos?: boolean;
}

export interface MapaSolucao {
  metricas: TipoMetrica[],
  itens: Item[]
}

export interface Camada {
  total: number;
  itens: Item[];
}
@Injectable({
  providedIn: 'root'
})
export class DataService {

  private mapaSolucao: MapaSolucao = {
    metricas: [],
    itens: []
  };

  constructor(private http: HttpClient) {
  }

  public async carregarDados(): Promise<any> {
    try {
      this.mapaSolucao = await this.http
        .get<MapaSolucao>(`${DADOS_URL}/mapa-solucao.json`)
        .toPromise();
      // this.tmpCalcularTotais();
      // this.tmpConverter(mapaSolucao.itens);
      return Promise.resolve();
    }
    catch (e) {
      return Promise.reject(e);
    }
  }

  getTiposMetricas(): TipoMetrica[] {
    return this.mapaSolucao.metricas;
  }

  getCamada(idItemPai: string, tipoMetrica: TipoMetrica): Camada {
    const itensCamada: Item[] = [];
    let totalCamada = 0;
    for (let item of this.mapaSolucao.itens) {
      if (item.idItemPai !== idItemPai) continue;
      const valor = item.metricas[tipoMetrica.id];
      if (item.totais) {
        totalCamada = valor;
      } else {
        item.temFilhos = this.temFilhos(item.id);
        itensCamada.push(item);
      }
    }
    const camada = this.camada(totalCamada, itensCamada, tipoMetrica)
    return camada;
  }

  private temFilhos(idItem: string): boolean {
    for (let item of this.mapaSolucao.itens) {
      if (item.idItemPai === idItem) return true;
    }
    return false;
  }

  private camada(totalCamada: number, itensCamada: Item[], tipoMetrica: TipoMetrica): Camada {
    return {
      total: totalCamada,
      itens: itensCamada
        .sort((a, b) => b[tipoMetrica.id] - a[tipoMetrica.id])
    };
  }

  // tmpCalcularTotais() {
  //   const ret = [];

  //   const itens: any[] = this.mapaSolucao.itens.filter(i => !i.totais).slice();
  //   itens.sort( (a,b) => +a.idItemPai - +b.idItemPai);

  //   let idPaiAnt = itens[0].idItemPai;
  //   let totais = {};
  //   itens.push({
  //     idItemPai: 'tail-' + Date.now(),
  //     metricas: {},
  //     tail: true
  //   })
  //   for (let item of itens) {
  //     if (item.idItemPai != idPaiAnt) {
  //       ret.push({
  //         idItemPai: idPaiAnt,
  //         totais: true,
  //         metricas: {...totais}
  //       })
  //       totais = {};
  //       idPaiAnt = item.idItemPai;
  //     }
  //     if (item.tail) {
  //       break;
  //     }
  //     for (let id in item.metricas) {
  //       let total = totais[id] | 0;
  //       totais[id] = total + item.metricas[id];
  //     }
  //     ret.push(item);
  //   }
  //   const str = JSON.stringify(ret);
  //   fileSaver.saveAs(new Blob([str], { type: 'text/json' }), 'totais.json');
  // }

  // tmpConverter(it) {
  //   const ret = [];
  //   const itens: any[] = it
  //   for (let item of itens) {
  //     const m = {};
  //     for (let metrica of item.metricas) {
  //       m[metrica.id] = metrica.valor;
  //     }
  //     item.metricas = m;
  //     ret.push(item);
  //   }
  //   const str = JSON.stringify(ret);
  //   fileSaver.saveAs(new Blob([str], { type: 'text/json' }), '~/dados.json');
  // }
}

