import * as cytoscape from 'cytoscape';
import { NodeSingular } from 'cytoscape';
import { Item, Metrica, OutrosDados, TipoMetrica } from '../data.service';
import { abreviarNumero } from './number-util';

export interface MetricNodeData {
    name: string;
    tipo: string;
    subtipo?: string;
    opacity: number;
}

export interface NodeData {
    id: string;
    name: string;
    metricaSelValor: number;
    nivelCamada: number;
    nomeGrupo: string;
    metricas: Metrica,
    outrosDados: OutrosDados;
    ordem: number;
    temFilhos: boolean;

    diametro?: number;
    opacity?: number;
    tipo?: string;
    xmargin?: number;
}
export interface Element {
    data: NodeData;
    position: cytoscape.Position;
    locked?: boolean;
    group: "nodes" | "edges"
}

export function itensToElements(pos, itens: Item[], level: number, metricaSel: TipoMetrica): Element[] {
    const nodes = itens.map(  (item, index) => itemToNode(index, pos, item, level, metricaSel));
    return [...nodes];
}

export interface Dado {
    valor: any;
    valorAbreviado?: string;
    descricao: any;
    icone?: string;
}
export interface Detalhes {
    titulo: string;
    node: NodeSingular;
    metricaAtual: Dado;
    dadosPrincipais: Dado[];
    dados: Dado[];
    temFilhos: boolean;
}

export function getDetalhes(node: NodeSingular, tipoMetricaSel: TipoMetrica, tiposMetrica: TipoMetrica[]): Detalhes {
    const data: NodeData = node.data()
    const detalhes: Detalhes = {
        titulo: data.nomeGrupo + ' - ' + data.name,
        node,
        temFilhos: data.temFilhos,
        metricaAtual: null,
        dadosPrincipais: [],
        dados: []
    };
    for (const id in data.metricas) {
        const tipoMetrica = tiposMetrica.find(m => m.id == id);
        const valor = data.metricas[id];
        const dado: Dado = {
            valor,
            valorAbreviado: abreviarNumero(valor),
            descricao: tipoMetrica.descricao,
            icone: tipoMetrica.icone,
        }
        if (tipoMetrica.id === tipoMetricaSel.id) {
            detalhes.metricaAtual = dado;
        } else if (tipoMetrica.principal) {
            detalhes.dadosPrincipais.push(dado);
        }
        detalhes.dados.push(dado);
    }

    return detalhes;
}


function itemToNode(ordem: number, pos, item: Item, nivelCamada: number, metricaSel: TipoMetrica): Element {
    return {
        data: {
            id: item.id,
            name: item.nome,
            metricaSelValor: item.metricas[metricaSel.id],
            nivelCamada,
            nomeGrupo: item.nomeGrupo,
            metricas: item.metricas,
            outrosDados: item.outrosDados,
            ordem,
            opacity: nivelCamada == 1 ? 0.2 : 0.6,
            temFilhos: item.temFilhos
        },
        position: {x: pos.x, y:pos.y},
        group: 'nodes'
    }
}

