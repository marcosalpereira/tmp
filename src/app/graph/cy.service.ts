import { Injectable } from '@angular/core';

import * as cy from 'cytoscape';
import { NodeSingular } from 'cytoscape';

import euler from 'cytoscape-euler';
import { Subject } from 'rxjs';
import { Camada, DataService, Item, TipoMetrica } from '../data.service';
cytoscape.use(euler);

import panzoom from 'cytoscape-panzoom';
import { itensToElements, MetricNodeData, NodeData } from './modelToCy';
import { abreviarNumero } from './number-util';

panzoom(cytoscape);

declare var cytoscape: any;


@Injectable({
  providedIn: 'root'
})
export class CyService {
  metricaSelecionada: TipoMetrica;

  private nodeClickedSubject = new Subject<NodeSingular>();
  nodeClicked$ = this.nodeClickedSubject.asObservable();

  public cy: cy.Core;
  constructor(
    private dataService: DataService
    ) { }

  public init(options): cy.Core {
    this.cy = cytoscape(options);

    if (window.innerWidth > 455) {
      (this.cy as any).panzoom({zoomOnly: true, animateOnFit: () => true});
    }

    return this.cy;
  }

  public async dataInit(): Promise<any> {
    return this.dataService.carregarDados();
  }

  getTiposMetricas(): TipoMetrica[] {
    return this.dataService.getTiposMetricas();
  }

  carregarCamada(id = '0'): number {
    const camada = this.dataService.getCamada(id, this.metricaSelecionada);
    if (!camada.total) return 0;

    this.loadItens(camada);
    this.eulerGraphLayout();
    return camada.total;

  }

  /**
   * Evitar que os os nós tenham diametros muito grande ou muito pequenos.
   * A ideia é saber o maior valor atualmente carregado e colocar os demais em proporção a este valor.
   * Evitando que fiquem abaixo de 10%.
   */
  private normalizarValores() {
    const max: number  = this.cy.nodes()
      .max(n => (n.data() as NodeData).metricaSelValor)
      .value;

    this.cy.nodes().forEach(n => {
      const data = n.data() as NodeData;
      const percentual = Math.floor(100.0 * data.metricaSelValor / max);
      let diametro = percentual;
      if (diametro < 10) diametro = 10;

      data.diametro =  diametro * 2;
      data.xmargin =  2;
    })
  }

  private loadItens(camada: Camada): cy.CollectionReturnValue {
    const pos = {
      x: this.cy.width()/2,
      y: this.cy.height()/2
    };

    const elements = itensToElements(pos, camada.itens, 1, this.metricaSelecionada);
    const added = this.loadGraph(elements);

    this.adicionarTotalCamada(camada.total);

    return added;
  }

  private adicionarTotalCamada(sum: number) {
    const pos = {
      x: this.cy.width()/6,
      y: -this.cy.height()/4
    };

    const metricaData: MetricNodeData = {
      name: abreviarNumero(sum),
      tipo: 'metrica',
      opacity: 1,
    }
    this.cy.add({
      data: metricaData,
      locked: true,
      position: { x: pos.x, y: pos.y },
      group: 'nodes'
    });

    const descricaoMetricaData: MetricNodeData = {
      name: this.metricaSelecionada.descricao.toUpperCase(),
      tipo: 'metrica',
      subtipo: 'descricao',
      opacity: 1,
    }
    this.cy.add({
      data: descricaoMetricaData,
      locked: true,
      position: { x: pos.x, y: pos.y + 30 },
      group: 'nodes'
    });
  }

  public find(valor: string): Promise<any> {
    const lc = valor.toLowerCase();
    const n = this.cy
      .nodes()
      .filter(ele => this.includesPredicate(ele, lc))
      .select();
    if (!n.length) {
      return Promise.reject('Não encontrado')
    }
    this.cy.fit(n, 10);
    return Promise.resolve(n.length + ' encontrado(s)');
  }

  private includesPredicate(ele: cy.NodeSingular, valor: string): boolean {
    return (ele.data() as NodeData).name.toLowerCase().includes(valor);
  }

  public fit(): Promise<any> {
    this.cy.fit(null, 10);
    return Promise.resolve();
  }

  private loadGraph(elements: any): cy.CollectionReturnValue {
    this.cy.reset();
    this.cy.startBatch();
    this.cy.elements().remove();
    const added = this.cy.add(elements);
    this.cy.endBatch();
    return added;
  }

  public eulerGraphLayout(): Promise<void> {
    this.normalizarValores();

    const elements = this.cy.elements();
    const options: any = {
      name: 'euler',
      // gravity: -3.0,
      animate: 'end'
    };
    elements.layout(options).run();
    return Promise.resolve();
  }

}
