export function abreviarNumero(valor) {
    if (valor == 0) { return '0'; }
    const k = 1000;
    const dm = 1;
    const i = Math.floor(Math.log(valor) / Math.log(k));
    const valorAbreviado = parseFloat((valor / Math.pow(k, i)).toFixed(dm));
    const sizes = valorAbreviado == 1
        ? ['', ' mil', ' milhão',  ' bilhão',  ' trilhão',  ' quatrilhão',  ' quintilhão',  ' sextilhão',  ' septilhão']
        : ['', ' mil', ' milhões', ' bilhões', ' trilhões', ' quatrilhões', ' quintilhões', ' sextilhões', ' septilhões']
    return valorAbreviado + sizes[i];
}

