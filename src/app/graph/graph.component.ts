import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { TipoMetrica } from '../data.service';

import { style as styles } from 'src/assets/styles.json';
import { CyService } from './cy.service';
import { NodeSingular } from 'cytoscape';
import { Detalhes, getDetalhes, NodeData } from './modelToCy';

interface BreacrumbItem {
  id: string;
  label: string;
}

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit {
  @ViewChild('cyContainerRef')
  cyContainerRef: ElementRef;

  messages: string[];
  tiposMetricas: TipoMetrica[];
  tipoMetricaSel: TipoMetrica;

  menuVisivel = true;
  query: string;
  segments: BreacrumbItem[] = []

  detalhes: Detalhes;

  timeoutHandler: number;
  mostrarInfoTamanho = true;
  ignoreTap: boolean;

  constructor(
    private cyService: CyService) { }

  ngOnInit() {
    this.initGraph();
    this.cyService.dataInit().then( () => {
      this.tiposMetricas = this.cyService.getTiposMetricas();
      this.tipoMetricaSel = this.tiposMetricas[0];
      this.cyService.metricaSelecionada = this.tipoMetricaSel;
      this.cyService.carregarCamada();
    });

    this.menuVisivel = window.innerWidth > 455;
  }

  showMessage(msg: string[], time = 3000) {
    this.messages = msg;
    setTimeout(() => {
      this.closeMessages();
    }, time);
  }

  closeMessages() {
    this.messages = null;
  }

  onClickMetrica(metrica) {
    this.tipoMetricaSel = metrica;
    this.cyService.metricaSelecionada = this.tipoMetricaSel;
    const id = this.segments.length
      ? this.segments[this.segments.length-1].id
      : '0';
    this.cyService.carregarCamada(id);
  }

  private initGraph() {
    const cyOptions = {
      container: document.getElementById('cy'),
      elements: [],
      boxSelectionEnabled: true,
      style: [...styles]
    };
    const cy = this.cyService.init(cyOptions);

    cy.on('tap', 'node', (evt) => {
      const node = evt.target;
      if (!this.ignoreTap) {
        this.mostrarDetalhes(node, evt);
      }
      this.ignoreTap = false;
    });

    cy.on('taphold', 'node', (evt) => {
      const node = evt.target;
      this.ignoreTap = true;
      this.navegar(node);
      setTimeout(() => {
        this.ignoreTap = false;
      }, 1000);
    });

  }

  navegar(node: NodeSingular): Promise<any> {
    const data: NodeData = node.data()

    const total = this.cyService.carregarCamada(data.id);
    if (total) {
      this.segments.push({
        id: data.id,
        label: data.name
      })
    }

    return Promise.resolve();
  }

  onBreacrumbClick(id: string) {
    const index = this.segments.findIndex(s => s.id == id);
    this.segments = this.segments.slice(0, index + 1);
    this.cyService.carregarCamada(id);
  }

  onSearch() {
    this.cyService
      .find(this.query)
      .then(msg => this.showMessage([msg]))
      .catch(err => this.showMessage([err]));
  }

  onClickScroll(e) {
    e.stopPropagation();
    const det = document.getElementById('detalhes-container');
    det.scrollBy({top: 200})
  }

  private mostrarDetalhes(node: NodeSingular, evt): Promise<any> {
    this.cyService.cy.userZoomingEnabled(false);

    this.detalhes = getDetalhes(node, this.tipoMetricaSel, this.tiposMetricas);

    // fazer apenas no proximo ciclo
    window.setTimeout(() => {
      const oe: MouseEvent = evt.originalEvent

      const x = oe.x ? oe.x : evt.renderedPosition.x;
      const y = oe.y ? oe.y : evt.renderedPosition.y;

      const det = document.getElementById('detalhes-principal');
      const tri = document.getElementById('triangle');

      const offsetY = 30;

      tri.style.left = x - 32 + 'px';

      det.style.display = 'block';

      let detLeft = Math.max(0, x - det.clientWidth/2);
      if (detLeft + det.clientWidth > window.innerWidth) {
        detLeft = window.innerWidth - det.clientWidth;
      }

      let triTop: number;
      let detTop = y + offsetY;
      if (detTop + det.clientHeight > window.innerHeight) {
        detTop = y - det.clientHeight - offsetY;
        triTop = y - offsetY/2 + 7 - 1;
        tri.classList.add('inverse')
      } else {
        triTop = y + offsetY + 1
        tri.classList.remove('inverse');
      }

      tri.style.top =  triTop + 'px';

      det.style.left = detLeft + 'px';
      det.style.top = detTop + 'px';

    }, 0);

    return Promise.resolve();

  }

  fecharDetalhes() {
    this.detalhes = null;
    this.cyService.cy.userZoomingEnabled(true);
  }

  fecharInfoTamanho() {
    this.mostrarInfoTamanho = false;
  }

}
