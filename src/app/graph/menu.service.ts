import { Injectable } from '@angular/core';
import { NodeSingular } from 'cytoscape';
import { Observable, Subject } from 'rxjs';
import { CyService } from './cy.service';

type CommandType = 'node' | 'core';

export type EventType =
    'detalhes'
  | 'navegar'
  | 'nodeLayout'
;

interface AcceptArgs {
  commandType: CommandType,
  selLength: number,
  element: NodeSingular,
  nodesLength: number,
}

export interface MenuSelectedCommand {
  type: EventType;
  arg?: any;
  evt?: any;
}

interface Command {
  id: EventType;
  content: string;
  accept: (args: AcceptArgs) => boolean
}

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private menuSelectedCommandSubject = new Subject<MenuSelectedCommand>();
  private menuSelectedCommand$ = this.menuSelectedCommandSubject.asObservable();

  private commands: Command[] = [
    {
      id: 'detalhes',
      content: 'Detalhes',
      accept: (args: AcceptArgs) =>
        args.commandType === 'node'
    },
    {
      id: 'navegar',
      content: 'Navegar',
      accept: (args: AcceptArgs) =>
        args.commandType === 'node'
    },
    {
      id: 'nodeLayout',
      content: 'Reposicionar Elementos',
      accept: (args: AcceptArgs) => args.commandType === 'node'
    }
  ];

  constructor(
    private cyService: CyService) {
  }

  private get cy() {
    return this.cyService.cy;''
  }

  public config$(): Observable<MenuSelectedCommand> {
    this.configMenus('core');
    this.configMenus('node');
    return this.menuSelectedCommand$;
  }

  private configMenus(commandType: CommandType) {
    const menu = {
      selector: commandType,
      menuRadius: 150,
      commands: (element) => {
        const selLength = this.cy.elements(':selected').length;
        const nodesLength = this.cy.nodes().length;
        return this.commands
          .filter(cmd => cmd.accept({commandType, selLength, element, nodesLength}))
          .map(cmd => this.mapCommand(cmd));
      }
    };
    const cya: any = this.cy;
    cya.cxtmenu(menu);
  }

  mapCommand(cmd): any {
    return {
      content: cmd.content,
      select: (ele: NodeSingular, evt) => {
        this.menuSelectedCommandSubject.next({ type: cmd.id, arg: ele, evt });
      }
    };
  }

}
